package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	"gitlab.com/goselect/gotoolchain"
)

func main() {
	cfg := gotoolchain.DefaultConfig()

	mgr := gotoolchain.NewManager(cfg)

	v, err := mgr.GetCurrentVerion()
	if err != nil {
		panic(err)
	} //if

	goroot := filepath.Join(cfg.ToolchainDir, v.String())

	switch cmd := filepath.Base(os.Args[0]); cmd {
	case "go", "go.exe":
		err = os.Setenv("GOROOT", goroot)
		if err != nil {
			panic(err)
		} //if

		c := exec.Command(filepath.Join(goroot, "bin", "go"), os.Args[1:]...)
		c.Stdin = os.Stdin
		c.Stdout = os.Stdout
		c.Stderr = os.Stderr
		err = c.Run()
		if err != nil {
			os.Exit(c.ProcessState.ExitCode())
		} //if
	default:
		fmt.Fprintln(os.Stderr, "error: cannot be used as a proxy for:", cmd)
		os.Exit(1)
	} //switch
} //func
