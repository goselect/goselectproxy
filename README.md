# goselectproxy

This is the command that proxies to the go command for goselect. It reads the
current version file in the goselect root. It sets the GOROOT env variable to
the root of the current Go toolchain.

## Building

This should not be called as `goselectproxy`. It is intended to be called as
`go`. Therefore a call to `go install gitlab.com/goselect/goselectproxy@latest`
won't make this usable. The executable should be built with `go build -o go`.

## Copyright

This is copyrighted and made freely available under the terms of the MIT
license.